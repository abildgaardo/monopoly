﻿namespace Matador
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            board = new PictureBox();
            rightPanel = new FlowLayoutPanel();
            flowLayoutPanel1 = new FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)board).BeginInit();
            rightPanel.SuspendLayout();
            SuspendLayout();
            // 
            // board
            // 
            board.Image = Properties.Resources.board;
            board.Location = new Point(0, 1);
            board.Name = "board";
            board.Size = new Size(1000, 1000);
            board.SizeMode = PictureBoxSizeMode.StretchImage;
            board.TabIndex = 0;
            board.TabStop = false;
            // 
            // rightPanel
            // 
            rightPanel.Controls.Add(flowLayoutPanel1);
            rightPanel.Location = new Point(997, 1);
            rightPanel.Name = "rightPanel";
            rightPanel.Size = new Size(188, 981);
            rightPanel.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Location = new Point(3, 3);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(200, 100);
            flowLayoutPanel1.TabIndex = 0;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1184, 981);
            Controls.Add(rightPanel);
            Controls.Add(board);
            Name = "Form1";
            Text = "HTX Monopoly";
            Load += Form1_Load;
            ((System.ComponentModel.ISupportInitialize)board).EndInit();
            rightPanel.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private PictureBox board;
        private FlowLayoutPanel rightPanel;
        private FlowLayoutPanel flowLayoutPanel1;
    }
}