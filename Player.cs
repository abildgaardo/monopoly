﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matador
{
    class Player
    {
        private string name;
        private int pos { get; set; }
        private int bankroll { get; set; }

        public Player(string name) {
            this.name = name;
            this.pos = 0;
            this.bankroll = 100;
        }
    }
}
