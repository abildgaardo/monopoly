﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matador
{
    class Grund : Felt
    {
        private int pris { get; set; }
        private int pantVaerdi { get; set; }
        private int pantIndloesVaerdi { get; set; }
        private bool pantet { get; set; } // Er grund pantsat TRUE/FALSE

        private int husPris { get; set; }
        private int hotelPris { get; set; }

        private int antalHuse { get; set; }
        private int antalHoteller { get; set; }

        private int[] leje { get; set; }
    }
}
