﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matador
{
    class Board
    {
        private Player[] player;
        private ChanceCard[] chanceCard;
        private CommunityChestCard[] communityChestCard;
        private List<Felt> felt;

        public Board() {
            //=== FELTER ( GRUNDE, REDDERIER, STATIONER & SYSTEMFELTER )
            //felt = new Grund[40];

            //== KORT ================================================
            chanceCard = new ChanceCard[16];
            communityChestCard = new CommunityChestCard[16];

            LoadJson();
            Debug.WriteLine("Antal felte i JSON: " + felt.Count());
            int counter = 0;
            foreach (Felt item in felt) {
                Debug.WriteLine( counter +": " + item.navn + " ("+ item.type +")");
                counter++;
            }
        }

        public void LoadJson()
        {
            using (StreamReader r = new StreamReader(@".\assets\monopoly_board.json"))
            {
                string json = r.ReadToEnd();
                felt = JsonConvert.DeserializeObject<List<Felt>>(json);
            }
        }
    }
}