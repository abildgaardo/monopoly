namespace Matador
{
    public partial class Form1 : Form
    {
        private int titleHeight;
        private int boardWidth = 1000;
        private int panelWidth = 200;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Rectangle screenRectangle = this.RectangleToScreen(this.ClientRectangle);

            titleHeight = screenRectangle.Top - this.Top;
            this.Size = new Size(boardWidth + panelWidth, boardWidth + titleHeight + 8);
            board.Size = new Size(boardWidth, boardWidth);

            new Board();
        }
    }
}